import {BaseEntity} from 'BaseEntity';
import {Vec2} from 'Vec2';
import {Render2d} from 'Render2d';

export class Explosion extends BaseEntity {
    protected t: number;
    protected duration: number;
    protected radius: number;

    public get currentRadius(): number {
        let x = this.t / this.duration;
        return (-Math.pow(2*x - 1, 4) + 1) * this.radius;
    }
 
    public constructor(position: Vec2, radius: number, duration: number) {
        super(position, new Vec2(0, 0));

        this.duration = duration;
        this.radius = radius;
        this.t = 0;
    }

    public update(delta: number): void {
        if (this.needsRemoval) {
            return;
        }

        this.t += delta;

        if (this.t >= this.duration) {
            this.needsRemoval = true;
        }
    }

    public render(renderer: Render2d): void {
        renderer.drawCircle(this.position, this.currentRadius, 'white');
    }
}
