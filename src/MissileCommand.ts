import {Game} from 'Game';
import {Missile} from 'Missile';
import {AIAttacker} from 'AIAttacker';
import {AIDefender} from 'AIDefender';
import {BaseEntity} from 'BaseEntity';
import {Explosion} from 'Explosion';
import {Vec2} from 'Vec2';
import {MissileCommandState} from 'MissileCommandState';
import {MissileCommandConfig} from 'MissileCommandConfig';

const config: MissileCommandConfig = {
    baseAmmo: 5,
    scalingAmmo: 1,
    attackerBaseAmmo: 5,
    attackerScalingAmmo: 1,
    missileSpeed: 1500,
    explosionRadius: 50,
    explosionDuration: 0.7,
    attackerMissileSpeed: 60,
    attackerExplosionRadius: 50,
    attackerExplosionDuration: 1,
    attackerBaseLaunchInterval: 1000,
    attackerScalingLaunchInterval: 50
}

export class MissileCommand extends Game {
    public state: MissileCommandState;
    private attackInterval: any;

    private defenderAI: AIDefender;

    private launchingAttack: boolean = false;
    private attackPivot: Vec2;
    private attackPoint: Vec2;

    public start() {
        super.start();
        this.state = new MissileCommandState(config);
        this.state.level = 1;
        //this.canvas.addEventListener('click', event =>  {
        //    this.launchDefenderMissile(new Vec2(event.x, event.y));
        //});
        this.canvas.addEventListener('mousedown', event => {
            if (this.launchingAttack) return;
            this.launchingAttack = true;
            this.attackPivot = new Vec2(event.x, event.y - 100);
            this.attackPoint = new Vec2(event.x, event.y);
        });
        this.canvas.addEventListener('mousemove', event => {
            if (!this.launchingAttack) return;
            let lineFn = this.lineBetweenPoints(this.attackPivot, new Vec2(event.x, event.y));
            let originX = lineFn(0);
            if (originX < 0) {
                this.attackPoint.x = 0;
                this.attackPoint.y = 0;
            } else if (originX > this.canvas.width) {
                this.attackPoint.x = this.canvas.width;
                this.attackPoint.y = 0;
            } else {
                this.attackPoint.x = event.x;
                this.attackPoint.y = event.y;
            }
        });
        this.canvas.addEventListener('mouseup', event => {
            if (!this.launchingAttack) return;
            let lineFn = this.lineBetweenPoints(this.attackPivot, this.attackPoint);
            let origin = new Vec2(lineFn(0), 0);
            let target = new Vec2(lineFn(this.canvas.height), this.canvas.height);
            this.launchAttackMissile(origin, target);
            this.launchingAttack = false;
        });
        this.setCursorType('crosshair');
        let AI = new AIAttacker(config, this.state, this.canvas);
        this.defenderAI = new AIDefender(config, this.state, this.canvas);
        this.defenderAI.start();
        AI.startAttackSequence();
    }

    public update(delta: number): void {
        this.updateMissiles(delta);
        this.updateExplosions(delta);
        this.checkMissileExplosionCollisions();
        this.defenderAI.update(delta);
    }

    public render(): void {
        for (let missile of this.state.p1_missiles) {
            missile.render(this.renderer);
        }
        for (let missile of this.state.p2_missiles) {
            missile.render(this.renderer);
        }
        for (let explosion of this.state.p1_explosions) {
            explosion.render(this.renderer);
        }
        for (let explosion of this.state.p2_explosions) {
            explosion.render(this.renderer);
        }
        if (this.launchingAttack) {
            let lineFn = this.lineBetweenPoints(this.attackPivot, this.attackPoint);
            let origin = new Vec2(lineFn(0), 0);
            let target = new Vec2(lineFn(this.canvas.height), this.canvas.height);
            this.renderer.drawDashedLine(origin, target, 'white');
            this.renderer.drawCircle(this.attackPivot, 3, 'red');
        }
    }

    private updateMissiles(delta: number): void {
        this.updateEntityList(delta, this.state.p1_missiles);
        this.updateEntityList(delta, this.state.p2_missiles);
    }

    private updateExplosions(delta: number): void {
        this.updateEntityList(delta, this.state.p1_explosions);
        this.updateEntityList(delta, this.state.p2_explosions);
    }

    private checkMissileExplosionCollisions(): void {
        for (let missile of this.state.p2_missiles) {
            for (let explosion of this.state.p1_explosions) {
                if (Vec2.distance(missile.position, explosion.position) <= explosion.currentRadius) {
                    missile.trigger();
                }
            }
            for (let explosion of this.state.p2_explosions) {
                if (Vec2.distance(missile.position, explosion.position) <= explosion.currentRadius) {
                    missile.trigger();
                }
            }
        }
    }

    private updateEntityList(delta: number, entities: BaseEntity[]): void {
        for (let i = 0; i < entities.length; i++) {
            let entity = entities[i];
            entity.update(delta);
            if (entity.needsRemoval) {
                entities.splice(i--, 1);
            }
        }
    }

    private lineBetweenPoints(p1: Vec2, p2: Vec2): any {
        if (p1.x === p2.x) {
            return (y: number) => p1.x;
        }
        let m = (p2.y - p1.y) / (p2.x - p1.x);
        return (y: number) => (y - p1.y + m * p1.x) / m;
    }

    private launchAttackMissile(origin: Vec2, target: Vec2): void {
        let m = new Missile(origin, target, config.attackerMissileSpeed);
        this.state.p2_missiles.push(m);
        m.detonate.once(pos => {
            let explosion = new Explosion(pos, config.attackerExplosionRadius, config.attackerExplosionDuration);
            this.state.p2_explosions.push(explosion);
        });
    }

    private launchDefenderMissile(target: Vec2): void {
        let source = new Vec2(this.canvas.width / 2, this.canvas.height);
        let m = new Missile(source, target, config.missileSpeed);
        this.state.p1_missiles.push(m);
        m.detonate.once(pos => {
            let explosion = new Explosion(pos, config.explosionRadius, config.explosionDuration);
            this.state.p1_explosions.push(explosion);
        });
    }
}
