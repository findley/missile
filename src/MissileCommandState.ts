import {Missile} from 'Missile';
import {Explosion} from 'Explosion';
import {MissileCommandConfig} from 'MissileCommandConfig';

export class MissileCommandState {
    private _level: number = 1;
    public buildings: any[] = [];
    public p1_missiles: Missile[] = [];
    public p2_missiles: Missile[] = [];
    public p1_explosions: Explosion[] = [];
    public p2_explosions: Explosion[] = [];
    public p1_ammo: number = 0;
    public p2_ammo: number = 0;

    protected config: MissileCommandConfig;

    public constructor(config: MissileCommandConfig, level: number = 1) {
        this.config = config;
        this.level = level;
    }

    public set level(level: number) {
        this._level = level;
        this.p1_missiles = [];
        this.p2_missiles = [];
        this.p1_explosions = [];
        this.p2_explosions = [];
        this.p1_ammo = this.config.baseAmmo + this.level * this.config.scalingAmmo;
        this.p2_ammo = this.config.attackerBaseAmmo + this.level * this.config.attackerScalingAmmo;
    }
}
