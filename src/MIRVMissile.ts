import {Missile} from 'Missile';
import {Vec2} from 'Vec2';

export class MIRVMissile extends Missile {
    private targets: Vec2[];

    public constructor(origin: Vec2, targets: Vec2[], speed: number, splitAltitude: number) {
        const averageX = targets.reduce((s, t) => s + t.x, 0) / targets.length;
        let target = new Vec2(averageX, splitAltitude);
        super(origin, target, speed);
        this.targets = targets;
    }
}
