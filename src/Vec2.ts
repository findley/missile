export class Vec2 {
    public x: number;
    public y: number;

    public constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public normalize(): Vec2 {
        const mag = Math.hypot(this.x, this.y);
        return this.divide(mag);
    }

    public magnitude(): number {
        return Math.hypot(this.x, this.y);
    }

    public addScalar(s: number): Vec2 {
        this.x += s;
        this.y += s;
        return this;
    }

    public addVector(v: Vec2): Vec2 {
        this.x += v.x;
        this.y += v.y;
        return this;
    }

    public subtractScalar(s: number): Vec2 {
        this.x -= s;
        this.y -= s;
        return this;
    }

    public subtractVector(v: Vec2): Vec2 {
        this.x -= v.x;
        this.y -= v.y;
        return this;
    }

    public multiply(scalar: number): Vec2 {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    public divide(scalar: number): Vec2 {
        this.x /= scalar;
        this.y /= scalar;
        return this;
    }

    public static distance(a: Vec2, b: Vec2): number {
        const dx = a.x - b.x;
        const dy = a.y - b.y;
        return Math.hypot(dx, dy);
    }

    public static multiply(v: Vec2, s: number): Vec2 {
        return new Vec2(v.x * s, v.y * s);
    }

    public static subtract(v1: Vec2, v2: Vec2): Vec2 {
        return new Vec2(v1.x - v2.x, v1.y - v2.y);
    }

    public static add(v1: Vec2, v2: Vec2): Vec2 {
        return new Vec2(v1.x + v2.x, v1.y + v2.y);
    }

    public static normalize(v: Vec2): Vec2 {
        let r = new Vec2(v.x, v.y);
        return r.normalize();
    }
}
