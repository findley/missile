import {Render2d} from 'Render2d';

export interface Entity {
    update(delta: number): void;
    render(renderer: Render2d): void;
}
