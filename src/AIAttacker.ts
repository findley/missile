import {Vec2} from 'Vec2';
import {Missile} from 'Missile';
import {Explosion} from 'Explosion';
import {MissileCommandConfig} from 'MissileCommandConfig';
import {MissileCommandState} from 'MissileCommandState';

export class AIAttacker {
    private _config: MissileCommandConfig;
    private _state: MissileCommandState;
    private _canvas: HTMLCanvasElement; 
    private _running: boolean;

    public constructor(config: MissileCommandConfig, state: MissileCommandState, canvas: HTMLCanvasElement) {
        this._canvas = canvas;
        this._config = config;
        this._state = state;
        this._running = false;
    }

    public startAttackSequence(): void {
        this._running = true;
        let interval = setInterval(() => {
            this.launchAttackMissile();
        }, this._config.attackerBaseLaunchInterval);
    }

    private launchAttackMissile(): void {
        let source = new Vec2(Math.random() * this._canvas.width, 0);
        let target = new Vec2(Math.random() * this._canvas.width, this._canvas.height);
        let m = new Missile(source, target, this._config.attackerMissileSpeed);
        this._state.p2_missiles.push(m);
        m.detonate.once(pos => {
            let explosion = new Explosion(pos, this._config.attackerExplosionRadius, this._config.attackerExplosionDuration);
            this._state.p2_explosions.push(explosion);
        });
    }
}
