import {Vec2} from 'Vec2';

export class Render2d {
    public context: CanvasRenderingContext2D;

    public constructor(context: CanvasRenderingContext2D) {
        this.context = context;
    }

    public drawLine(s: Vec2, t: Vec2, color: string) {
        this.context.beginPath();
        this.context.setLineDash([]);
        this.context.moveTo(s.x, s.y);
        this.context.lineTo(t.x, t.y);
        this.context.strokeStyle = color;
        this.context.stroke();
    }

    public drawDashedLine(s: Vec2, t: Vec2, color: string) {
        this.context.beginPath();
        this.context.setLineDash([5, 15]);
        this.context.moveTo(s.x, s.y);
        this.context.lineTo(t.x, t.y);
        this.context.strokeStyle = color;
        this.context.stroke();
    }

    public drawCircle(pos: Vec2, r: number, color: string) {
        if (r < 0) return;
        this.context.beginPath();
        this.context.arc(pos.x, pos.y, r, 0, Math.PI * 2);
        this.context.fillStyle = color;
        this.context.fill();
    }
}

