import {Render2d} from 'Render2d';

export abstract class Game {
    public canvas: HTMLCanvasElement;
    public renderer: Render2d;
    public running: boolean = false;
    protected lastTime: number;

    public abstract update(delta: number): void;
    public abstract render(): void;

    public start() {
        this.lastTime = window.performance.now();
        this.canvas = window.document.createElement('canvas');
        this.canvas.id = 'canvas';
        this.canvas.height = window.innerHeight;
        this.canvas.width = window.innerWidth;
        let context = this.canvas.getContext('2d');
        if (!context) {
            return
        }
        this.renderer = new Render2d(context);
        this.running = true;

        window.document.body.appendChild(this.canvas);
        window.addEventListener('resize', this.handleResize.bind(this));
        window.requestAnimationFrame(this.loop.bind(this));
    }

    protected loop(time: number) {
        if (!this.running) return;

        let delta = (time - this.lastTime) / 1000;
        this.lastTime = time;

        this.renderer.context.fillStyle = 'midnightblue';
        this.renderer.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.render();
        this.update(delta);

        window.requestAnimationFrame(this.loop.bind(this));
    }

    protected handleResize() {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
    }

    protected setCursorType(cursor: string): void {
        this.canvas.style.cursor = cursor;
    }
}
