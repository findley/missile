import {BaseEntity} from 'BaseEntity';
import {Vec2} from 'Vec2';
import {EventEmitter} from 'Event';
import {Render2d} from 'Render2d';

export class Missile extends BaseEntity {
    public originPosition: Vec2;
    public targetPosition: Vec2;
    public get detonate() { return this._detonate.expose(); }

    protected t: number;
    protected flightTime: number;
    protected _color: string;

    protected readonly _detonate: EventEmitter<Vec2>;

    public constructor(origin: Vec2, target: Vec2, speed: number) {
        let path = Vec2.subtract(target, origin);
        let dist = path.magnitude();
        let velocity = Vec2.normalize(path).multiply(speed);

        super(origin, velocity);

        this.originPosition = origin;
        this.targetPosition = target;
        this.t = 0;
        this.flightTime = dist / speed;
        this._detonate = new EventEmitter<Vec2>();
    }

    public update(delta: number): void {
        if (this.needsRemoval) {
            return;
        }

        this.t += delta;
        let displacement = Vec2.multiply(this.velocity, this.t);
        this.position = Vec2.add(this.originPosition, displacement);

        if (this.t >= this.flightTime) {
            this.trigger();
        }
    }

    public timeToTarget(): number {
        return this.flightTime - this.t;
    }

    public trigger(): void {
        this._detonate.trigger(this.position);
        this.needsRemoval = true;
    }

    public render(renderer: Render2d): void {
        renderer.drawLine(this.originPosition, this.position, 'white');
        renderer.drawCircle(this.targetPosition, 2, 'white');
    }
}
