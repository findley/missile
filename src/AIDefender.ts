import {Vec2} from 'Vec2';
import {Missile} from 'Missile';
import {Explosion} from 'Explosion';
import {MissileCommandConfig} from 'MissileCommandConfig';
import {MissileCommandState} from 'MissileCommandState';

export class AIDefender {
    private _config: MissileCommandConfig;
    private _state: MissileCommandState;
    private _canvas: HTMLCanvasElement; 
    private _running: boolean;
    private _timeToLaunch: number;

    public constructor(config: MissileCommandConfig, state: MissileCommandState, canvas: HTMLCanvasElement) {
        this._canvas = canvas;
        this._config = config;
        this._state = state;
        this._running = false;
    }

    public start(): void {
        this._timeToLaunch = 0;
        this._running = true;
    }

    public update(delta: number): void {
        this._timeToLaunch -= delta;
        if (this._timeToLaunch <= 0) {
            this.defend();
            this._timeToLaunch = 1;
        }
    }

    private defend(): void {
        let result = this.findBiggestThreat();
        if (result.threatFound && result.missile) {
            let target = this.predictIntercept(result.missile);
            this.launch(target);
        }
    }

    private predictIntercept(missile: Missile): Vec2 {
        return Vec2.add(missile.position, missile.velocity);
    }

    private launch(target: Vec2): void {
        let source = new Vec2(this._canvas.width / 2, this._canvas.height);
        let m = new Missile(source, target, this._config.missileSpeed);
        this._state.p1_missiles.push(m);
        m.detonate.once(pos => {
            let explosion = new Explosion(pos, this._config.explosionRadius, this._config.explosionDuration);
            this._state.p1_explosions.push(explosion);
        });
    }

    private findBiggestThreat(): {threatFound: boolean, missile?: Missile} {
        let result: any = {
            threatFound: false
        };
        let lowestTimeToTarget = Number.MAX_SAFE_INTEGER;
        for (let missile of this._state.p2_missiles) {
            let timeToTarget = missile.timeToTarget();
            if (timeToTarget < lowestTimeToTarget) {
                result.threatFound = true;
                result.missile = missile;
                lowestTimeToTarget = timeToTarget;
            }
        }
        return result;
    }
}
