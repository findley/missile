export interface MissileCommandConfig {
    baseAmmo: number;
    scalingAmmo: number;
    attackerBaseAmmo: number;
    attackerScalingAmmo: number;
    missileSpeed: number;
    explosionRadius: number;
    explosionDuration: number;
    attackerMissileSpeed: number;
    attackerExplosionRadius: number;
    attackerExplosionDuration: number;
    attackerBaseLaunchInterval: number;
    attackerScalingLaunchInterval: number;
}
