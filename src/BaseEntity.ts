import {Entity} from 'Entity';
import {Vec2} from 'Vec2';
import {Render2d} from 'Render2d';

export abstract class BaseEntity implements Entity {
    public position: Vec2;
    public velocity: Vec2;
    public needsRemoval: boolean;

    public constructor(position: Vec2, velocity: Vec2) {
        this.position = position;
        this.velocity = velocity;
        this.needsRemoval = false;
    }

    public abstract update(delta: number): void;
    public abstract render(renderer: Render2d): void;

    protected updatePosition(delta: number): void {
        this.position.addVector(Vec2.multiply(this.velocity, delta));
    }
}
