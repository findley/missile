export interface IEventEmitter<T> {
    on(handler: {(data: T): void}): void;
    once(handler: {(data: T): void}): void;
    off(handler: {(data: T): void}): void;
}

export class EventEmitter<T> implements IEventEmitter<T> {
    private handlers: { (data: T): void;}[] = [];
    private onceHandlers: { (data?: T): void;}[] = [];

    public on(handler: {(data: T): void}): void {
        this.handlers.push(handler);
    }

    public once(handler: {(data: T): void}): void {
        this.onceHandlers.push(handler);
    }

    public off(handler: {(data: T): void}): void {
        this.handlers = this.handlers.filter(h => h !== handler);
        this.onceHandlers = this.onceHandlers.filter(h => h !== handler);
    }

    public trigger(data: T) {
        this.handlers.slice(0).forEach(h => h(data));
        this.onceHandlers.slice(0).forEach(h => h(data));
        this.onceHandlers = [];
    }

    public expose(): IEventEmitter<T> {
        return this;
    }
}
