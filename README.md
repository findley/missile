# Missile

Missile Command implemented with HTML5 canvas and javscript (TypeScript).

### Prerequisites

To build the project you will need nodeJS and NPM. The only dependencies are development dependencies for compiling typescript and bundling it with webpack.

### Installing

First clone the repository.

```
git clone https://gitlab.com/findley/missile.git
```

Then install the dev dependencies.

```
npm install
```

Build the project with NPM.

```
npm run build
```

The output will be in ./dist

Alternatively run in development mode which watches for changes to files in ./src and hosts the files on a local webserver.

```
npm run dev
```

## Deployment

Host the static files in ./dist, or open ./dist/index.html directly

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
